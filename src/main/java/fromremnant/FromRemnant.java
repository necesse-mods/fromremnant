package fromremnant;

import fromremnant.armor.fieldmedic.*;
import fromremnant.armor.leto.*;
import fromremnant.armor.nightstalker.*;
import fromremnant.buffs.*;
import fromremnant.buffs.setbonus.*;
import fromremnant.food.*;
import fromremnant.materials.*;
import fromremnant.projectiles.*;
import fromremnant.tech.*;
import fromremnant.weapons.magic.*;
import fromremnant.weapons.melee.*;
import fromremnant.weapons.ranged.*;
import necesse.engine.modLoader.annotations.ModEntry;
import necesse.engine.modifiers.ModifierValue;
import necesse.engine.registries.RecipeTechRegistry;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.entity.mobs.buffs.staticBuffs.ShownCooldownBuff;
import necesse.entity.mobs.buffs.staticBuffs.SimpleModifierBuff;
import necesse.entity.mobs.buffs.staticBuffs.armorBuffs.setBonusBuffs.SimpleSetBonusBuff;
import necesse.entity.mobs.buffs.staticBuffs.armorBuffs.trinketBuffs.SimpleTrinketBuff;
import necesse.entity.mobs.hostile.bosses.MotherSlimeMob;
import necesse.inventory.item.Item;
import necesse.inventory.item.trinketItem.SimpleTrinketItem;
import necesse.inventory.lootTable.lootItem.ChanceLootItem;
import necesse.inventory.lootTable.lootItem.LootItem;
import necesse.inventory.lootTable.lootItem.LootItemList;
import necesse.inventory.lootTable.lootItem.OneOfTicketLootItems;
import necesse.inventory.lootTable.presets.DeepCaveChestLootTable;
import necesse.inventory.lootTable.presets.DeepCrateLootTable;
import necesse.inventory.recipe.Ingredient;
import necesse.inventory.recipe.Recipe;

import static necesse.engine.registries.BuffRegistry.registerBuff;
import static necesse.engine.registries.ItemRegistry.registerItem;
import static necesse.engine.registries.ObjectRegistry.registerObject;
import static necesse.engine.registries.ProjectileRegistry.registerProjectile;
import static necesse.engine.registries.RecipeTechRegistry.registerTech;
import static necesse.inventory.recipe.Recipes.registerModRecipe;

@ModEntry
public class FromRemnant {

    public void initResources() {
    }

    public void init() {
        System.out.println("From Remnant Loaded");

        // register
        /* objects */
        registerObject("steelanvil", new SteelAnvil(), 20.0F, true);
        /* tech */
        registerTech("steelanvil");
        /* materials */
        registerItem("steelbar", new SteelBar(), 12, true);
        /* food */
        registerItem("celestialthaenfruit", new CelestialThaenFruit(), 4000, true);
        /* projectiles */
        registerProjectile("smolderflame", SmolderProjectile.class, "smolderflame", null);
        registerProjectile("crescentmoonprojectile", CrescentMoonProjectile.class, "frostarrow", null);
        registerProjectile("nightfallprojectile", NightfallProjectile.class, "frostbullet", null);
        /* weapons */
        registerItem("steelsword", new SteelSword(), 500.0F, true);
        registerItem("smolder", new Smolder(), 620.0F, true);
        registerItem("gasgiant", new GasGiant(), 620.0F, true);
        registerItem("dreamcatcherstaff", new DreamcatcherStaff(), 620.0F, true);
        registerItem("crescentmoon", new CrescentMoon(), 640.0F, true);
        registerItem("nightfall", new Nightfall(), 680.0F, true);
        registerItem("plasmacutter", new PlasmaCutter(), 680.0F, true);
        /* armor */
        registerItem("letohelmet", new LetoHelmet(), 300.0F, true);
        registerItem("letochestplate", new LetoChestPlate(), 330.0F, true);
        registerItem("letoboots", new LetoBoots(), 280.0F, true);

        registerItem("nightstalkershroud", new NightStalkerShroud(), 300.0F, true);
        registerItem("nightstalkergarb", new NightStalkerGarb(), 330.0F, true);
        registerItem("nightstalkerboots", new NightStalkerBoots(), 280.0F, true);

        registerItem("fieldmedichood", new FieldMedicHood(), 300.0F, true);
        registerItem("fieldmedicovercoat", new FieldMedicOvercoat(), 330.0F, true);
        registerItem("fieldmedicboots", new FieldMedicBoots(), 280.0F, true);
        /* trinkets */
        registerItem("letoamulet", new SimpleTrinketItem(Item.Rarity.RARE, "letoamuletbuff",800), 500.0F, true);
        registerItem("probabilitycord", new SimpleTrinketItem(Item.Rarity.RARE, "probabilitycordbuff",800), 500.0F, true);
        registerItem("firestonering", new SimpleTrinketItem(Item.Rarity.RARE, "firestoneringbuff",700), 450.0F, true);
        registerItem("heartofthewolf", new SimpleTrinketItem(Item.Rarity.RARE, "heartofthewolfbuff",700), 450.0F, true);

        // buffs
        /* food */
        registerBuff("celestialthaenfruitbuff", new CelestialThaenFruitBuff());
        /* weapons */
        registerBuff("smolderbuff", new SmolderBuff());
        registerBuff("gasgiantbuff", new GasGiantBuff());
        registerBuff("dreamcatcherstaffbuff", new DreamCatcherStaffBuff());
        registerBuff("crescentmooncooldown", new ShownCooldownBuff());
        registerBuff("crescentmoonactive", new SimpleModifierBuff(true, true, true, true, new ModifierValue<>(BuffModifiers.RANGED_ATTACK_SPEED, 0.15F)));
        registerBuff("nightfallcooldown", new ShownCooldownBuff());
        registerBuff("nightfallactive", new SimpleModifierBuff(true, true, true, true, new ModifierValue<>(BuffModifiers.RANGED_ATTACK_SPEED, 0.3F)));
        /* armor */
        registerBuff("letosetbonus", new SimpleSetBonusBuff(new ModifierValue<>(BuffModifiers.ARMOR_FLAT, 30)));
        registerBuff("nightstalkersetbonus", new NightStalkerSet());
        registerBuff("nightstalkeractive", new NightStalkerActive());
        registerBuff("fieldmedicsetbonus", new FieldMedicSet());
        registerBuff("fieldmediccooldown", new ShownCooldownBuff());
        registerBuff("fieldmedicactive", new SimpleModifierBuff(true, true, true, true, new ModifierValue<>(BuffModifiers.HEALTH_REGEN_FLAT, 5.0F), new ModifierValue<>(BuffModifiers.COMBAT_HEALTH_REGEN_FLAT, 5.0F)));
        /* trinkets */
        registerBuff("letoamuletbuff", new SimpleTrinketBuff(new ModifierValue<>(BuffModifiers.SPEED, 0.25F), new ModifierValue<>(BuffModifiers.ATTACK_MOVEMENT_MOD, 0.0F)));
        registerBuff("probabilitycordbuff", new SimpleTrinketBuff(new ModifierValue<>(BuffModifiers.CRIT_DAMAGE, 0.3F)));
        registerBuff("firestoneringbuff", new SimpleTrinketBuff("firestoneringtip",new ModifierValue<>(BuffModifiers.MAGIC_DAMAGE, 0.1F), new ModifierValue<>(BuffModifiers.FIRE_DAMAGE).max(0.5F)));
        registerBuff("heartofthewolfbuff", new SimpleTrinketBuff(new ModifierValue<>(BuffModifiers.STAMINA_REGEN, 0.25F), new ModifierValue<>(BuffModifiers.STAMINA_CAPACITY, 0.5F)));
    }

    public void postInit() {
        // crates loot
        DeepCaveChestLootTable.extraItems.items.add(
                new LootItemList(
                        new ChanceLootItem(0.05F, "firestonering"),
                        new ChanceLootItem(0.05F, "heartofthewolf")
                ));
        DeepCrateLootTable.basicDeepCrate.oneOfItems.addAll(
                new OneOfTicketLootItems(
                        new ChanceLootItem(0.05F, "firestonering"),
                        new ChanceLootItem(0.05F, "heartofthewolf")
                ));
        DeepCaveChestLootTable.basicBars.add(
                LootItem.offset("steelbar", 3, 2));
        DeepCaveChestLootTable.snowBars.add(
                LootItem.offset("steelbar", 5, 1));
        DeepCaveChestLootTable.swampBars.add(
                LootItem.offset("steelbar", 6, 2));
        DeepCaveChestLootTable.desertBars.add(
                LootItem.offset("steelbar", 8, 2));

        DeepCrateLootTable.basicBars.add(
                LootItem.between("steelbar", 3, 5));
        DeepCrateLootTable.snowBars.add(
                LootItem.between("steelbar", 4, 6));
        DeepCrateLootTable.swampBars.add(
                LootItem.between("steelbar", 5, 8));
        DeepCaveChestLootTable.desertBars.add(
                LootItem.between("steelbar", 6, 10));

        // boss loot
        MotherSlimeMob.uniqueDrops.items.add(new LootItem("celestialthaenfruit"));

        // recipes
        /* objects */
        registerModRecipe(new Recipe(
                "steelanvil",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("steelbar", 6)
                }).showAfter("advancedworkstation")
        );
        /* materials */
        registerModRecipe(new Recipe(
                "steelbar",
                4,
                RecipeTechRegistry.FORGE,
                new Ingredient[]{
                        new Ingredient("ironbar", 4),
                        new Ingredient("bone", 1)
                })
        );
        /* weapons */
        registerModRecipe(new Recipe(
                "steelsword",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("bone", 8),
                        new Ingredient("steelbar", 12)
                }).showAfter("hardenedshield")
        );
        registerModRecipe(new Recipe(
                "smolder",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("shadowessence", 10),
                        new Ingredient("steelbar", 12)
                }).showAfter("steelsword")
        );
        registerModRecipe(new Recipe(
                "crescentmoon",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("glacialshard", 8),
                        new Ingredient("steelbar", 12)
                }).showAfter("letoboots")
        );
        registerModRecipe(new Recipe(
                "nightfall",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("cryoessence", 10),
                        new Ingredient("steelbar", 12)
                }).showAfter("crescentmoon")
        );
        registerModRecipe(new Recipe(
                "gasgiant",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("bioessence", 10),
                        new Ingredient("steelbar", 16)
                }).showAfter("nightstalkerboots")
        );
        registerModRecipe(new Recipe(
                "dreamcatcherstaff",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("primordialessence", 10),
                        new Ingredient("steelbar", 16)
                }).showAfter("fieldmedicboots")
        );
        registerModRecipe(new Recipe(
                "plasmacutter",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("primordialessence", 10),
                        new Ingredient("steelbar", 12)
                }).showAfter("dreamcatcherstaff")
        );
        /* armor */
        registerModRecipe(new Recipe(
                "letohelmet",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("steelbar", 12),
                        new Ingredient("leather", 4)
                }).showAfter("smolder")
        );
        registerModRecipe(new Recipe(
                "letochestplate",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("steelbar", 16),
                        new Ingredient("leather", 8)
                }).showAfter("letohelmet")
        );
        registerModRecipe(new Recipe(
                "letoboots",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("steelbar", 8),
                        new Ingredient("leather", 4)
                }).showAfter("letochestplate")
        );

        registerModRecipe(new Recipe(
                "nightstalkershroud",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("leather", 12),
                        new Ingredient("glacialbar", 4)
                }).showAfter("nightfall")
        );
        registerModRecipe(new Recipe(
                "nightstalkergarb",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("leather", 16),
                        new Ingredient("glacialbar", 8)
                }).showAfter("nightstalkershroud")
        );
        registerModRecipe(new Recipe(
                "nightstalkerboots",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("leather", 8),
                        new Ingredient("glacialbar", 4)
                }).showAfter("nightstalkergarb")
        );

        registerModRecipe(new Recipe(
                "fieldmedichood",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("silk", 8),
                        new Ingredient("myceliumbar", 4)
                }).showAfter("gasgiant")
        );
        registerModRecipe(new Recipe(
                "fieldmedicovercoat",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("leather", 16),
                        new Ingredient("myceliumbar", 8)
                }).showAfter("fieldmedichood")
        );
        registerModRecipe(new Recipe(
                "fieldmedicboots",
                1,
                RecipeTechRegistry.getTech("steelanvil"),
                new Ingredient[]{
                        new Ingredient("leather", 8),
                        new Ingredient("myceliumbar", 4)
                }).showAfter("fieldmedicovercoat")
        );
    }
}