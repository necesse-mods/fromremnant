package fromremnant.weapons.ranged;

import fromremnant.buffs.BuffManager;
import fromremnant.projectiles.CrescentMoonProjectile;
import necesse.engine.localization.Localization;
import necesse.engine.network.PacketReader;
import necesse.engine.util.GameBlackboard;
import necesse.entity.mobs.GameDamage;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.projectile.Projectile;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.PlayerInventorySlot;
import necesse.inventory.item.ItemInteractAction;
import necesse.inventory.item.arrowItem.ArrowItem;
import necesse.inventory.item.toolItem.projectileToolItem.bowProjectileToolItem.BowProjectileToolItem;
import necesse.inventory.item.upgradeUtils.FloatUpgradeValue;
import necesse.level.maps.Level;

public class CrescentMoon extends BowProjectileToolItem implements ItemInteractAction {
    public CrescentMoon() {
        super(1850);
        this.attackAnimTime.setBaseValue(500);
        this.rarity = Rarity.EPIC;
        this.attackDamage = new FloatUpgradeValue(72, 0.3F).setBaseValue(72).setUpgradedValue(1, 96);
        this.velocity.setBaseValue(200);
        this.attackRange.setBaseValue(650);
        this.attackXOffset = 12;
        this.attackYOffset = 31;
        this.resilienceGain.setBaseValue(0).setUpgradedValue(1, 0.5F);
    }

    public boolean canLevelInteract(Level level, int x, int y, PlayerMob player, InventoryItem item) {
        return !player.buffManager.hasBuff("crescentmooncooldown");
    }

    public float getItemCooldownPercent(InventoryItem item, PlayerMob perspective) {
        return perspective.buffManager.getBuffDurationLeftSeconds("crescentmooncooldown");
    }

    public InventoryItem onLevelInteract(Level level, int x, int y, PlayerMob player, int attackHeight, InventoryItem item, PlayerInventorySlot slot, int seed, PacketReader contentReader) {
        BuffManager.applyBuffs(player, new String[]{"crescentmooncooldown","crescentmoonactive"}, new float[]{25.0F, 5.0F});
        return item;
    }

    public Projectile getProjectile(Level level, int x, int y, Mob owner, InventoryItem item, int seed, ArrowItem arrow, boolean consumeAmmo, float velocity, int range, GameDamage damage, int knockback, float resilienceGain, PacketReader contentReader) {
        return new CrescentMoonProjectile(level, owner.x, owner.y, (float)x, (float)y, velocity, range, damage, knockback, owner);
    }

    public ListGameTooltips getPreEnchantmentTooltips(InventoryItem item, PlayerMob perspective, GameBlackboard blackboard) {
        ListGameTooltips tooltips = super.getPreEnchantmentTooltips(item, perspective, blackboard);
        tooltips.add(Localization.translate("itemtooltip", "crescentmoontip"));
        return tooltips;
    }
}
