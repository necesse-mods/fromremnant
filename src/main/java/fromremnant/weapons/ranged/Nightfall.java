package fromremnant.weapons.ranged;

import fromremnant.buffs.BuffManager;
import fromremnant.projectiles.NightfallProjectile;
import necesse.engine.Screen;
import necesse.engine.localization.Localization;
import necesse.engine.network.PacketReader;
import necesse.engine.sound.SoundEffect;
import necesse.engine.util.GameBlackboard;
import necesse.entity.mobs.AttackAnimMob;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.projectile.Projectile;
import necesse.gfx.GameResources;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.PlayerInventorySlot;
import necesse.inventory.item.ItemInteractAction;
import necesse.inventory.item.bulletItem.BulletItem;
import necesse.inventory.item.toolItem.projectileToolItem.gunProjectileToolItem.GunProjectileToolItem;
import necesse.inventory.item.upgradeUtils.IntUpgradeValue;
import necesse.level.maps.Level;

public class Nightfall extends GunProjectileToolItem implements ItemInteractAction {

    public Nightfall() {
        super(NORMAL_AMMO_TYPES, 1920);
        this.rarity = Rarity.EPIC;
        this.attackAnimTime.setBaseValue(600);
        this.attackDamage.setBaseValue(86).setUpgradedValue(1, 106);
        this.attackXOffset = 20;
        this.attackYOffset = 10;
        this.velocity.setBaseValue(650);
        this.moveDist = 65;
        this.attackRange.setBaseValue(1200);
        this.resilienceGain.setBaseValue(0).setUpgradedValue(1, 0.5F);
        this.addGlobalIngredient("bulletuser");
    }

    public boolean canLevelInteract(Level level, int x, int y, PlayerMob player, InventoryItem item) {
        return !player.buffManager.hasBuff("nightfallcooldown");
    }

    public float getItemCooldownPercent(InventoryItem item, PlayerMob perspective) {
        return perspective.buffManager.getBuffDurationLeftSeconds("nightfallcooldown");
    }

    public InventoryItem onLevelInteract(Level level, int x, int y, PlayerMob player, int attackHeight, InventoryItem item, PlayerInventorySlot slot, int seed, PacketReader contentReader) {
        IntUpgradeValue percent = new IntUpgradeValue()
                .setBaseValue(5)
                .setUpgradedValue(3, 10);

        ActiveBuff buff = new ActiveBuff("nightfallactive", player, 5F, player);
        buff.getGndData().setInt("percent", percent.getValue(getUpgradeTier(item)));

        BuffManager.applyBuff(player, "nightfallcooldown", 25.0F);
        player.buffManager.addBuff(buff, true);
        return item;
    }

    public void playFireSound(AttackAnimMob mob) {
        Screen.playSound(GameResources.fizz, SoundEffect.effect(mob).volume(0.5F).pitch(2.0F));
    }

    public Projectile getProjectile(InventoryItem item, BulletItem bulletItem, float x, float y, float targetX, float targetY, int range, Mob owner) {
        return new NightfallProjectile(x, y, targetX, targetY, bulletItem.modVelocity(getProjectileVelocity(item, owner)), range, bulletItem.modDamage(getAttackDamage(item)), bulletItem.modKnockback(getKnockback(item, owner)), owner);
    }

    public ListGameTooltips getPreEnchantmentTooltips(InventoryItem item, PlayerMob perspective, GameBlackboard blackboard) {
        int percent = (getUpgradeTier(item) >= 3) ? 10 : 5;
        ListGameTooltips tooltips = super.getPreEnchantmentTooltips(item, perspective, blackboard);
        tooltips.add(Localization.translate("itemtooltip", "nightfalltip", "percent", percent));
        return tooltips;
    }
}
