package fromremnant.weapons.magic;

import fromremnant.buffs.BuffManager;
import necesse.engine.localization.Localization;
import necesse.engine.util.GameBlackboard;
import necesse.engine.util.GameUtils;
import necesse.entity.levelEvent.toolItemEvent.ToolItemEvent;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.PlayerMob;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.toolItem.swordToolItem.SwordToolItem;
import necesse.level.maps.Level;

public class DreamcatcherStaff extends SwordToolItem {

    public DreamcatcherStaff() {
        super(1800);
        this.rarity = Rarity.EPIC;
        this.attackAnimTime.setBaseValue(650);
        this.attackDamage.setBaseValue(58).setUpgradedValue(1,92);
        this.attackRange.setBaseValue(72);
        this.knockback.setBaseValue(75);
        this.resilienceGain.setBaseValue(0).setUpgradedValue(1, 2);
    }

    public void hitMob(InventoryItem item, ToolItemEvent event, Level level, Mob target, Mob attacker) {
        super.hitMob(item, event, level, target, attacker);

        float distX, distY;
        if (attacker.dir == 0) {
            // top
            distX = attacker.x;
            distY = attacker.y-this.attackRange.getValue(0);
        } else if (attacker.dir == 1) {
            // left
            distX = attacker.x+this.attackRange.getValue(0);
            distY = attacker.y;
        } else if (attacker.dir == 2) {
            // down
            distX = attacker.x;
            distY = attacker.y+this.attackRange.getValue(0);
        } else if (attacker.dir == 3) {
            // right
            distX = attacker.x-this.attackRange.getValue(0);
            distY = attacker.y;
        } else {
            distY = attacker.y;
            distX = attacker.x;
        }

        attacker.getLevel().entityManager.mobs.streamInRegionsShape(GameUtils.rangeBounds(attacker.x, attacker.y, this.attackRange.getValue(0)), 0)
                .filter((m) -> !m.removed())
                .filter((m) -> m.getDistance(distX, distY) <= 48)
                .filter((m) -> !m.isHuman)
                .forEach((m) -> BuffManager.applyBuff(m,"dreamcatcherstaffbuff",4.0F, attacker, true));
    }

    public ListGameTooltips getPreEnchantmentTooltips(InventoryItem item, PlayerMob perspective, GameBlackboard blackboard) {
        ListGameTooltips tooltips = super.getPreEnchantmentTooltips(item, perspective, blackboard);
        tooltips.add(Localization.translate("itemtooltip", "dreamcatcherstafftip"));
        return tooltips;
    }
}
