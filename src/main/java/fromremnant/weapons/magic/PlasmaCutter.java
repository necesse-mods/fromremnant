package fromremnant.weapons.magic;

import necesse.engine.Screen;
import necesse.engine.localization.Localization;
import necesse.engine.network.PacketReader;
import necesse.engine.network.packet.PacketLevelEvent;
import necesse.engine.network.server.ServerClient;
import necesse.engine.sound.SoundEffect;
import necesse.engine.util.GameBlackboard;
import necesse.engine.util.GameRandom;
import necesse.entity.levelEvent.mobAbilityLevelEvent.MouseBeamLevelEvent;
import necesse.entity.mobs.AttackAnimMob;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.mobs.attackHandler.MouseBeamAttackHandler;
import necesse.gfx.GameResources;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.PlayerInventorySlot;
import necesse.inventory.item.toolItem.projectileToolItem.magicProjectileToolItem.MagicProjectileToolItem;
import necesse.level.maps.Level;

import java.awt.*;

public class PlasmaCutter extends MagicProjectileToolItem {
    public PlasmaCutter() {
        super(1920);
        this.rarity = Rarity.EPIC;
        this.attackAnimTime.setBaseValue(2000);
        this.attackDamage.setBaseValue(24).setUpgradedValue(1, 28);
        this.attackXOffset = 16;
        this.attackYOffset = 10;
        this.velocity.setBaseValue(120);
        this.attackRange.setBaseValue(400);
        this.manaCost.setBaseValue(2);
        this.resilienceGain.setBaseValue(0).setUpgradedValue(1, 1);
    }

    public void showAttack(Level level, int x, int y, AttackAnimMob mob, int attackHeight, InventoryItem item, int seed, PacketReader contentReader) {
        if (level.isClient()) {
            Screen.playSound(GameResources.magicbolt4, SoundEffect.effect(mob).volume(0.1F).pitch(GameRandom.globalRandom.getFloatBetween(0.6F, 0.7F)));
        }
    }

    public InventoryItem onAttack(Level level, int x, int y, PlayerMob player, int attackHeight, InventoryItem item, PlayerInventorySlot slot, int animAttack, int seed, PacketReader contentReader) {
        MouseBeamLevelEvent event = new MouseBeamLevelEvent(player, x, y, seed, 50.0F, this.getAttackRange(item), this.getAttackDamage(item), this.getKnockback(item, player), 250, 1, 0, this.getResilienceGain(item), new Color(234, 100, 63));
        level.entityManager.addLevelEventHidden(event);
        this.consumeMana(player, item);
        player.startAttackHandler(new MouseBeamAttackHandler(player, slot, 50, seed, event, 1.0F));
        if (level.isServer()) {
            ServerClient client = player.getServerClient();
            level.getServer().network.sendToClientsWithEntityExcept(new PacketLevelEvent(event), event, client);
        }
        return item;
    }

    public ListGameTooltips getPreEnchantmentTooltips(InventoryItem item, PlayerMob perspective, GameBlackboard blackboard) {
        ListGameTooltips tooltips = super.getPreEnchantmentTooltips(item, perspective, blackboard);
        tooltips.add(Localization.translate("itemtooltip", "plasmacuttertip"));
        return tooltips;
    }

}
