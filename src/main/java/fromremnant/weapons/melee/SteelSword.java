package fromremnant.weapons.melee;

import necesse.inventory.item.toolItem.swordToolItem.SwordToolItem;

public class SteelSword extends SwordToolItem {

    public SteelSword() {
        super(1500);
        this.rarity = Rarity.RARE;
        this.attackAnimTime.setBaseValue(500);
        this.attackDamage.setBaseValue(50).setUpgradedValue(1, 90);
        this.attackRange.setBaseValue(64);
        this.knockback.setBaseValue(25);
        this.resilienceGain.setBaseValue(1);
    }

}
