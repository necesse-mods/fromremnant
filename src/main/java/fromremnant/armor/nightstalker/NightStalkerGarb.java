package fromremnant.armor.nightstalker;

import necesse.engine.modifiers.ModifierValue;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.ChestArmorItem;

public class NightStalkerGarb extends ChestArmorItem {
    public NightStalkerGarb() {
        super(20, 1100, Item.Rarity.RARE, "nightstalkergarb", "nightstalkerarms");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.RANGED_DAMAGE, 0.1F)
        );
    }
}
