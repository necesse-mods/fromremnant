package fromremnant.armor.nightstalker;

import necesse.engine.modifiers.ModifierValue;
import necesse.engine.registries.DamageTypeRegistry;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.SetHelmetArmorItem;

public class NightStalkerShroud extends SetHelmetArmorItem {
    public NightStalkerShroud() {
        super(16, DamageTypeRegistry.RANGED, 1100, Item.Rarity.RARE, "nightstalkershroud", "nightstalkergarb", "nightstalkerboots", "nightstalkersetbonus");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.RANGED_CRIT_CHANCE, 0.2F),
                new ModifierValue<>(BuffModifiers.RANGED_CRIT_DAMAGE, 0.2F)
        );
    }
}