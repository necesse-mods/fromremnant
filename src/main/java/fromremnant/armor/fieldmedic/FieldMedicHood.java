package fromremnant.armor.fieldmedic;

import necesse.engine.modifiers.ModifierValue;
import necesse.engine.registries.DamageTypeRegistry;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.SetHelmetArmorItem;

public class FieldMedicHood extends SetHelmetArmorItem {
    public FieldMedicHood() {
        super(15, DamageTypeRegistry.NORMAL, 1100, Item.Rarity.RARE, "fieldmedichood", "fieldmedicovercoat", "fieldmedicboots", "fieldmedicsetbonus");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.CRIT_CHANCE, 0.1F),
                new ModifierValue<>(BuffModifiers.ATTACK_SPEED, 0.1F)
        );
    }
}
