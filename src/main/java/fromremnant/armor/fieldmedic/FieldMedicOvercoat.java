package fromremnant.armor.fieldmedic;

import necesse.engine.modifiers.ModifierValue;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.ChestArmorItem;

public class FieldMedicOvercoat extends ChestArmorItem {
    public FieldMedicOvercoat() {
        super(19, 1100, Item.Rarity.RARE, "fieldmedicovercoat", "fieldmedicarms");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.ALL_DAMAGE, 0.05F)
        );
    }
}
