package fromremnant.armor.leto;

import necesse.engine.modifiers.ModifierValue;
import necesse.engine.registries.DamageTypeRegistry;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.SetHelmetArmorItem;

public class LetoHelmet extends SetHelmetArmorItem {
    public LetoHelmet() {
        super(22, DamageTypeRegistry.NORMAL, 1100, Item.Rarity.RARE, "letohelmet", "letochestplate", "letoboots", "letosetbonus");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.FRICTION, 0.2F),
                new ModifierValue<>(BuffModifiers.SPEED, -0.07F),
                new ModifierValue<>(BuffModifiers.SWIM_SPEED, -0.07F)
        );
    }
}
