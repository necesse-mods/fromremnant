package fromremnant.armor.leto;

import necesse.engine.modifiers.ModifierValue;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.ChestArmorItem;

public class LetoChestPlate extends ChestArmorItem {
    public LetoChestPlate() {
        super(30, 1100, Item.Rarity.RARE, "letochestplate", "letoarms");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.FRICTION, 0.4F),
                new ModifierValue<>(BuffModifiers.SPEED, -0.11F),
                new ModifierValue<>(BuffModifiers.SWIM_SPEED, -0.11F)
        );
    }
}
