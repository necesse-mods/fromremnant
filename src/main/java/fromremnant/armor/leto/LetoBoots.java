package fromremnant.armor.leto;

import necesse.engine.modifiers.ModifierValue;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.armorItem.ArmorModifiers;
import necesse.inventory.item.armorItem.BootsArmorItem;

public class LetoBoots extends BootsArmorItem {
    public LetoBoots() {
        super(18, 1100, Item.Rarity.RARE, "letoboots");
    }

    public ArmorModifiers getArmorModifiers(InventoryItem item, Mob mob) {
        return new ArmorModifiers(
                new ModifierValue<>(BuffModifiers.FRICTION, 0.2F),
                new ModifierValue<>(BuffModifiers.SPEED, -0.07F),
                new ModifierValue<>(BuffModifiers.SWIM_SPEED, -0.07F)
        );
    }
}
