package fromremnant.food;

import fromremnant.buffs.BuffManager;
import necesse.engine.localization.Localization;
import necesse.engine.util.GameBlackboard;
import necesse.entity.mobs.PlayerMob;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.item.Item;
import necesse.inventory.item.placeableItem.consumableItem.food.FoodConsumableItem;
import necesse.level.maps.Level;
import necesse.level.maps.levelData.settlementData.settler.Settler;

public class CelestialThaenFruit extends FoodConsumableItem {
    public CelestialThaenFruit() {
        super(1, Rarity.EPIC, Settler.FOOD_SIMPLE, 10,0);
    }

    public String canConsume(Level level, PlayerMob player, InventoryItem item) {
        BuffManager.applyBuff(player,"celestialthaenfruitbuff",3600.0F);
        return null;
    }

    public Item setItemCategory(String... categoryTree) {
        return super.setItemCategory("consumable", "rawfood");
    }

    public ListGameTooltips getTooltips(InventoryItem item, PlayerMob perspective, GameBlackboard blackboard) {
        ListGameTooltips tooltips = super.getTooltips(item, perspective, blackboard);
        tooltips.add(Localization.translate("itemtooltip", "celestialthaenfruittip"));
        return tooltips;
    }
}
