package fromremnant.projectiles;

import necesse.entity.mobs.GameDamage;
import necesse.entity.mobs.Mob;
import necesse.entity.projectile.bulletProjectile.BulletProjectile;
import necesse.entity.trails.Trail;
import necesse.gfx.GameResources;
import necesse.gfx.gameTexture.GameSprite;
import necesse.level.maps.LevelObjectHit;

import java.awt.*;

public class NightfallProjectile extends BulletProjectile {
    public NightfallProjectile() {}

    public NightfallProjectile(float x, float y, float targetX, float targetY, float speed, int distance, GameDamage damage, int knockback, Mob owner) {
        super(x, y, targetX, targetY, speed, distance, damage, knockback, owner);
    }

    public void doHitLogic(Mob target, LevelObjectHit object, float x, float y) {
        super.doHitLogic(target, object, x, y);
        if (this.isServer()) {
            if (target != null && target.isHostile) {
                Mob owner = getOwner();
                if (owner.buffManager.hasBuff("nightfallactive")) {
                    int percent = (owner.buffManager.getBuff("nightfallactive").getGndData().getInt("percent", 5) * owner.getMaxHealth()) / 100;
                    final int addHealth = owner.getHealth() > owner.getMaxHealth() - percent ? owner.getMaxHealth() - owner.getHealth() : percent;
                    owner.setHealth(owner.getHealth() + addHealth);
                }
            }
        }
    }

    public Trail getTrail() {
        Trail trail = new Trail(this, this.getLevel(), new Color(77, 105, 150, 185), 22.0f, 100, this.getHeight());
        trail.sprite = new GameSprite(GameResources.chains, 7, 0, 32);
        return trail;
    }

    protected Color getWallHitColor() {
        return new Color(77, 105, 150);
    }

}
