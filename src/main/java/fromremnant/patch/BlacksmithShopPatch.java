package fromremnant.patch;

import necesse.engine.modLoader.annotations.ModMethodPatch;
import necesse.engine.network.server.ServerClient;
import necesse.engine.util.GameRandom;
import necesse.entity.mobs.friendly.human.humanShop.BlacksmithHumanMob;
import necesse.level.maps.levelData.villageShops.ShopItem;
import necesse.level.maps.levelData.villageShops.VillageShopsData;
import net.bytebuddy.asm.Advice;

import java.util.ArrayList;

@ModMethodPatch(target = BlacksmithHumanMob.class, name = "getShopItems", arguments = {VillageShopsData.class, ServerClient.class})
public class BlacksmithShopPatch {

    @Advice.OnMethodExit
    static void onExit(@Advice.This BlacksmithHumanMob blacksmith, ServerClient client, @Advice.Return(readOnly = false) ArrayList<ShopItem> shopItems) {

        if (shopItems == null) {
            return;
        }
        GameRandom random = new GameRandom(blacksmith.getShopSeed() + 5L);
        if (client.characterStats().mob_kills.getKills("ancientvulture") > 0) {
            shopItems.add(ShopItem.item("letoamulet", blacksmith.getRandomHappinessPrice(random, 500, 800, 150)));
        }
    }

}
