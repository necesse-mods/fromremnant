package fromremnant.patch;

import necesse.engine.modLoader.annotations.ModMethodPatch;
import necesse.engine.network.server.ServerClient;
import necesse.engine.util.GameRandom;
import necesse.entity.mobs.friendly.human.humanShop.HunterHumanMob;
import necesse.level.maps.levelData.villageShops.ShopItem;
import necesse.level.maps.levelData.villageShops.VillageShopsData;
import net.bytebuddy.asm.Advice;

import java.util.ArrayList;

@ModMethodPatch(target = HunterHumanMob.class, name = "getShopItems", arguments = {VillageShopsData.class, ServerClient.class})
public class HunterShopPatch {
    @Advice.OnMethodExit
    static void onExit(@Advice.This HunterHumanMob hunter, ServerClient client, @Advice.Return(readOnly = false) ArrayList<ShopItem> shopItems) {

        if (shopItems == null) {
            return;
        }
        GameRandom random = new GameRandom(hunter.getShopSeed() + 5L);
        if (client.characterStats().mob_kills.getKills("reaper") > 0) {
            shopItems.add(ShopItem.item("probabilitycord", hunter.getRandomHappinessPrice(random, 600, 1000, 200)));
        }
    }
}
