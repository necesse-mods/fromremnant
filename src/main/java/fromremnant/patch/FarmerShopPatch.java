package fromremnant.patch;

import necesse.engine.modLoader.annotations.ModMethodPatch;
import necesse.engine.network.server.ServerClient;
import necesse.engine.util.GameRandom;
import necesse.entity.mobs.friendly.human.humanShop.FarmerHumanMob;
import necesse.level.maps.levelData.villageShops.ShopItem;
import necesse.level.maps.levelData.villageShops.VillageShopsData;
import net.bytebuddy.asm.Advice;

import java.util.ArrayList;

@ModMethodPatch(target = FarmerHumanMob.class, name = "getShopItems", arguments = {VillageShopsData.class, ServerClient.class})
public class FarmerShopPatch {
    @Advice.OnMethodExit
    static void onExit(@Advice.This FarmerHumanMob farmer, ServerClient client, @Advice.Return(readOnly = false) ArrayList<ShopItem> shopItems) {

        if (shopItems == null) {
            return;
        }
        GameRandom random = new GameRandom(farmer.getShopSeed() + 5L);
        if (client.characterStats().mob_kills.getKills("cryoqueen") > 0) {
            shopItems.add(ShopItem.item("celestialthaenfruit", farmer.getRandomHappinessPrice(random, 4000, 6000, 500)));
        }
    }
}
