package fromremnant.buffs.setbonus;

import fromremnant.buffs.BuffManager;
import necesse.engine.Settings;
import necesse.engine.control.Control;
import necesse.engine.localization.Localization;
import necesse.engine.network.Packet;
import necesse.engine.network.PacketReader;
import necesse.engine.network.PacketWriter;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.buffs.ActiveBuffAbility;
import necesse.entity.mobs.buffs.BuffEventSubscriber;
import necesse.entity.mobs.buffs.staticBuffs.StaminaBuff;
import necesse.entity.mobs.buffs.staticBuffs.armorBuffs.setBonusBuffs.SetBonusBuff;
import necesse.gfx.camera.GameCamera;
import necesse.gfx.gameTooltips.ListGameTooltips;
import necesse.inventory.InventoryItem;
import necesse.inventory.PlayerInventorySlot;

public class NightStalkerSet extends SetBonusBuff implements ActiveBuffAbility {
    public NightStalkerSet() {}

    public void init(ActiveBuff buff, BuffEventSubscriber eventSubscriber) {
    }
    public void onItemAttacked(ActiveBuff buff, int targetX, int targetY, PlayerMob player, int attackHeight, InventoryItem item, PlayerInventorySlot slot, int animAttack) {
        super.onItemAttacked(buff, targetX, targetY, player, attackHeight, item, slot, animAttack);
    }

    public Packet getStartAbilityContent(PlayerMob player, ActiveBuff buff, GameCamera camera) {
        return this.getRunningAbilityContent(player, buff);
    }

    public Packet getRunningAbilityContent(PlayerMob player, ActiveBuff buff) {
        Packet content = new Packet();
        PacketWriter writer = new PacketWriter(content);
        StaminaBuff.writeStaminaData(player, writer);
        return content;
    }

    public boolean canRunAbility(PlayerMob player, ActiveBuff buff, Packet packet) {
        if (buff.owner.isRiding()) {
            return false;
        } else {
            return player.isServer() && Settings.giveClientsPower || StaminaBuff.canStartStaminaUsage(buff.owner);
        }
    }

    public void onActiveAbilityStarted(PlayerMob player, ActiveBuff buff, Packet packet) {
        PacketReader reader = new PacketReader(packet);
        if (!player.isServer() || Settings.giveClientsPower) {
            StaminaBuff.readStaminaData(player, reader);
        }

        BuffManager.applyBuff(player,"nightstalkeractive",1.0F);
    }

    public boolean tickActiveAbility(PlayerMob player, ActiveBuff buff, boolean isRunningClient) {
        if (player.inLiquid()) {
            player.buffManager.removeBuff("nightstalkeractive", false);
        } else {
            ActiveBuff activeBuff = player.buffManager.getBuff("nightstalkeractive");
            if (activeBuff != null) {
                activeBuff.setDurationLeftSeconds(1.0F);
            } else {
                player.buffManager.addBuff(new ActiveBuff("nightstalkeractive", player, 1.0F, null), false);
            }

            long msToDeplete = 4000L;
            float usage = 50.0F / (float)msToDeplete;
            if (!StaminaBuff.useStaminaAndGetValid(player, usage)) {
                return false;
            }
        }

        return !isRunningClient || Control.SET_ABILITY.isDown();
    }

    public void onActiveAbilityUpdate(PlayerMob playerMob, ActiveBuff buff, Packet packet) {
    }

    public void onActiveAbilityStopped(PlayerMob player, ActiveBuff buff) {
        player.buffManager.removeBuff("nightstalkeractive", false);

    }

    public ListGameTooltips getTooltip(ActiveBuff ab) {
        ListGameTooltips tooltips = new ListGameTooltips();
        tooltips.add(Localization.translate("itemtooltip", "nightstalkerset"));
        return tooltips;
    }
}
