package fromremnant.buffs.setbonus;

import fromremnant.buffs.BuffManager;
import necesse.engine.localization.Localization;
import necesse.engine.network.Packet;
import necesse.engine.util.GameMath;
import necesse.engine.util.GameRandom;
import necesse.engine.util.GameUtils;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.buffs.BuffAbility;
import necesse.entity.mobs.buffs.BuffEventSubscriber;
import necesse.entity.mobs.buffs.staticBuffs.armorBuffs.setBonusBuffs.SetBonusBuff;
import necesse.entity.particle.Particle;
import necesse.gfx.gameTooltips.ListGameTooltips;

import java.awt.*;
import java.util.concurrent.atomic.AtomicReference;

public class FieldMedicSet extends SetBonusBuff implements BuffAbility {
    private final int range = 128;

    public FieldMedicSet() {
    }

    public void init(ActiveBuff buff, BuffEventSubscriber eventSubscriber) {
    }

    public void tickEffect(ActiveBuff buff, Mob owner) {
        super.tickEffect(buff, owner);
        if (owner.isVisible() && buff.owner.buffManager.hasBuff("fieldmedicactive")) {
            GameRandom random = GameRandom.globalRandom;
            AtomicReference<Float> currentAngle = new AtomicReference<>(random.nextFloat() * 360);
            owner.getLevel().entityManager.addParticle(owner.x, owner.y-48, Particle.GType.IMPORTANT_COSMETIC)
                    .color(new Color(101, 168, 101))
                    .height(-32.0F)
                    .moves((pos, delta, lifeTime, timeAlive, lifePercent) -> {
                        float angle = currentAngle.accumulateAndGet(delta * 0.02F, Float::sum);
                        pos.x = owner.x + GameMath.sin(angle) * this.range;
                        pos.y = (owner.y-48) + GameMath.cos(angle) * this.range;
                    })
                    .lifeTime(1000)
                    .sizeFades(16, 24);

            owner.getLevel().entityManager.addParticle(owner.x, owner.y-48, Particle.GType.IMPORTANT_COSMETIC)
                    .color(new Color(101, 168, 101))
                    .givesLight(64, 1)
                    .height(-32.0F)
                    .sizeFades(this.range, this.range)
                    .dontRotate();
        }
    }

    public void runAbility(PlayerMob player, ActiveBuff buff, Packet packet) {
            GameUtils.streamNetworkClients(player.getLevel())
                    .filter((c) -> c.playerMob.getDistance(player.x, player.y) <= (float)this.range)
                    .filter((c) -> BuffManager.shouldBuffPlayer(player, c.playerMob))
                    .forEach((c) -> BuffManager.applyBuffs(c.playerMob, new String[]{"fieldmediccooldown","fieldmedicactive"}, new float[]{70.0F,10.0F}));

            player.getLevel().entityManager.mobs.streamInRegionsShape(GameUtils.rangeBounds(player.x, player.y, this.range), 0)
                    .filter((m) -> !m.removed())
                    .filter((m) -> m.getDistance(player.x, player.y) <= (float)this.range)
                    .filter(BuffManager::shouldBuffSettler)
                    .forEach((m) -> BuffManager.applyBuffs(m, new String[]{"fieldmediccooldown","fieldmedicactive"}, new float[]{70.0F,10.0F}));
    }

    public boolean canRunAbility(PlayerMob player, ActiveBuff buff, Packet packet) {
        return !buff.owner.buffManager.hasBuff("fieldmediccooldown");
    }

    public ListGameTooltips getTooltip(ActiveBuff buff) {
        ListGameTooltips tooltips = new ListGameTooltips();
        tooltips.add(Localization.translate("itemtooltip", "fieldmedicset"));
        return tooltips;
    }
}
