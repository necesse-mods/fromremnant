package fromremnant.buffs.setbonus;

import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.buffs.BuffEventSubscriber;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.entity.mobs.buffs.staticBuffs.Buff;

public class NightStalkerActive extends Buff {
    public NightStalkerActive() {
        this.shouldSave = false;
        this.isVisible = false;
    }

    public void init(ActiveBuff buff, BuffEventSubscriber eventSubscriber) {
        buff.setMinModifier(BuffModifiers.SLOW, 1.0F, 1000000);
        buff.setModifier(BuffModifiers.RANGED_DAMAGE, 1.0F);
    }

}
