package fromremnant.buffs;

import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.buffs.BuffEventSubscriber;
import necesse.entity.mobs.buffs.BuffModifiers;
import necesse.entity.mobs.buffs.staticBuffs.Buff;

public class DreamCatcherStaffBuff extends Buff {

    public DreamCatcherStaffBuff() {
        this.canCancel = false;
        this.isImportant = true;
    }

    public void init(ActiveBuff buff, BuffEventSubscriber buffEventSubscriber) {
        buff.setMinModifier(BuffModifiers.SLOW, 0.9F, 1000000);
    }
}
