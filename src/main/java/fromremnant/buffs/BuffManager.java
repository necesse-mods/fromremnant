package fromremnant.buffs;

import necesse.entity.mobs.Attacker;
import necesse.entity.mobs.Mob;
import necesse.entity.mobs.PlayerMob;
import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.friendly.human.HumanMob;

public class BuffManager {

    public static boolean shouldBuffPlayer(PlayerMob player, PlayerMob target) {
        return player == target || player.isSameTeam(target);
    }

    public static boolean shouldBuffSettler(Mob target) {
        if (!(target instanceof HumanMob)) {
            return false;
        }
        return ((HumanMob) target).isSettler();
    }

    public static boolean shouldBuffAdventureParty(PlayerMob player, Mob target) {
        if (!(target instanceof HumanMob)) {
            return false;
        }
        if (player.isServer()) {
            return player.getServerClient().adventureParty.contains((HumanMob) target);
        }
        return player.getClient().adventureParty.contains((HumanMob) target);
    }

    /**
     * Add ActiveBuff
     */
    public static void applyBuff(Mob mob, String buff, float cooldown) {
        mob.buffManager.addBuff(new ActiveBuff(buff, mob, cooldown, null), false);
    }

    /**
     * Add ActiveBuff with source and send packet
     */
    public static void applyBuff(Mob mob, String buff, float cooldown, Attacker source, boolean sendUpdatePacket) {
        mob.buffManager.addBuff(new ActiveBuff(buff, mob, cooldown, source), sendUpdatePacket);
    }

    /**
     * Add ActiveBuffs
     */
    public static void applyBuffs(Mob mob, String[] buffs, float[] cooldown) {
        for (int i=0; i < buffs.length; i++) {
            mob.buffManager.addBuff(new ActiveBuff(buffs[i], mob, cooldown[i], null), false);
        }
    }
}
