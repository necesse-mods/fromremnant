package fromremnant.buffs;

import necesse.engine.network.packet.PacketLifelineEvent;
import necesse.entity.mobs.MobBeforeHitCalculatedEvent;
import necesse.entity.mobs.buffs.ActiveBuff;
import necesse.entity.mobs.buffs.BuffEventSubscriber;
import necesse.entity.mobs.buffs.staticBuffs.Buff;
import necesse.level.maps.Level;

public class CelestialThaenFruitBuff extends Buff {
    public CelestialThaenFruitBuff() {
    }

    public void init(ActiveBuff activeBuff, BuffEventSubscriber buffEventSubscriber) {
    }

    public void onBeforeHitCalculated(ActiveBuff buff, MobBeforeHitCalculatedEvent event) {
        super.onBeforeHitCalculated(buff, event);
        Level level = buff.owner.getLevel();
        if (level.isServer() && event.getExpectedHealth() <= 0) {
            event.prevent();
            buff.owner.setHealth(Math.max(10, buff.owner.getMaxHealth() / 2));
            buff.owner.buffManager.removeBuff("celestialthaenfruitbuff", true);
            level.getServer().network.sendToClientsWithTile(new PacketLifelineEvent(buff.owner.getUniqueID()), level, (int)buff.owner.x, (int)buff.owner.y);
        }
    }

    public void loadTextures() {
        super.loadTextures();
    }
}
