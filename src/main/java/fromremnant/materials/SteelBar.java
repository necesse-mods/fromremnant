package fromremnant.materials;

import necesse.inventory.item.matItem.MatItem;

public class SteelBar extends MatItem {

    public SteelBar() {
        super(100, Rarity.UNCOMMON);
    }
}
